import css from './css/index.css' // 引入CSS
import less from './css/black.less' // 引入Less
import sass from './css/black2.scss' // 引入Sass

var json = require('../config.json') // 引入json

import $ from 'jquery' // 引入JQuery（局部引入）
{
    let title = "Hello Webpack! Server OK!";
    // document.getElementById("title").innerHTML = title; // DOM方式
    $('#title').html(title); // JQuery方式

    document.getElementById("json").innerHTML = json.name; // 使用json
}