const path = require('path');
const uglify = require('uglifyjs-webpack-plugin'); // JS压缩~插件
const htmlPlugin = require('html-webpack-plugin'); // html打包~插件
const extractTextPlugin = require('extract-text-webpack-plugin'); // css分离~插件

const glob = require('glob');
const purifyCSSPlugin = require('purifycss-webpack'); // 清除未使用CSS

const entry = require('./webpack_config/entry_wepack.js'); // 引入：入口配置模块文件
const copyWebpackPlugin = require('copy-webpack-plugin'); // 插件：输出静态资源

const webpack = require('webpack');

if (process.env.type == "build") { // 生产环境
    var website = { // 解决：CSS分离后，图片路径访问问题（采用绝对路径访问）
        publicPath: "http://192.168.3.34:8087/"
    }
} else { // 开发环境
    var website = { // 解决：CSS分离后，图片路径访问问题（采用绝对路径访问）
        publicPath: "http://localhost:8087/"
    }
}

module.exports = {
    // devtool: 'eval-source-map', // 开发调试 source-map
    // entry: { // 入口配置
    //     entry: './src/entry.js'
    // },
    entry: entry.path, // 入口模块配置
    output: { // 出口配置
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js', // 出口文件名同入口文件名
        publicPath: website.publicPath
    },
    module: {
        rules: [
            // 规则：打包CSS（分离CSS，自动处理CSS3前缀）
            {
                test: /\.css$/,
                use: extractTextPlugin.extract({ // 分离CSS
                    fallback: "style-loader",
                    // use: "css-loader"
                    use: [{
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1
                            }
                        },
                        'postcss-loader'
                    ]
                })
            },
            // 规则：打包CSS中的图片
            {
                test: /\.(png|jpg|gif)/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 1024, // 500000
                        outputPath: 'images/',
                    }
                }]
            },
            // 规则：加载HTML中图片
            {
                test: /\.(htm|html)$/i,
                use: ['html-withimg-loader']
            },
            // 规则：打包Less
            {
                test: /\.less$/,
                use: extractTextPlugin.extract({
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "less-loader"
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            },
            // 规则：打包Sass
            {
                test: /\.scss$/,
                use: extractTextPlugin.extract({
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "sass-loader"
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            },
            // 规则：babel转换
            {
                test: /\.(jsx|js)$/,
                use: {
                    loader: 'babel-loader',
                },
                exclude: /node_modules/ // 去除指定文件夹
            }
        ]
    },
    plugins: [ // 插件
        new uglify(), // 插件：js压缩
        new htmlPlugin({ // 插件：html打包
            minify: {
                removeAttributeQuotes: true, // 去掉HTML标签双引号
            },
            hash: true, // 开启哈希（防止缓存不更新）
            template: './src/index.html' // HTML所在路径
        }),
        new extractTextPlugin("css/index.css"), // 插件：CSS分离
        new purifyCSSPlugin({ // 插件：清除未使用CSS
            paths: glob.sync(path.join(__dirname, 'src/*.html'))
        }),
        new webpack.ProvidePlugin({ // 引入JQuery（全局引入）
            $: "jquery"
        }),
        new webpack.BannerPlugin('我是版权声明！！！我是版权声明！！！我是版权声明！！！'), // 插件：版权申明
        new webpack.optimize.CommonsChunkPlugin({ // 插件：抽离jquery库
            name: ['jquery', 'vue'], // name对应入口文件中的名字，我们起的是jQuery
            filename: "assets/js/[name].min.js", // 把文件打包到哪里，是一个路径 
            minChunks: 2 // 最小打包的文件模块数，这里直接写2就好
        }),
        new copyWebpackPlugin([{
            from: __dirname + '/src/public',
            to: './public'
        }])
    ],
    devServer: { // 配置开发服务功能
        contentBase: path.resolve(__dirname, 'dist'),
        host: 'localhost', // localhost
        compress: true,
        port: 8087 // 80
    },
    watchOptions: { // watch配置项
        poll: 1000, // 检测修改的时间，以毫秒为单位
        aggregateTimeout: 500, // 防止重复保存而发生重复编译错误。这里设置的500是半秒内重复保存，不进行打包操作
        ignored: /node_modules/, //不监听的目录
    }
}